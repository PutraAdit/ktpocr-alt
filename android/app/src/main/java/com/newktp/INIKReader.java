package com.newktp;

import android.graphics.Bitmap;

public interface INIKReader {
    void open(String modelPath, String language);

    String read(Bitmap image) throws Exception;

    void close();
}
