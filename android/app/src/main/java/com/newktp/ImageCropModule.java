package com.newktp;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.ColorMatrix;
import android.graphics.ColorMatrixColorFilter;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.util.Base64;
import android.util.Log;

import com.facebook.react.bridge.Arguments;
import com.facebook.react.bridge.Promise;
import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.bridge.ReactContextBaseJavaModule;
import com.facebook.react.bridge.ReactMethod;
import com.facebook.react.bridge.WritableMap;
import com.newktp.preprocessing.ImagePreprocessor;

import java.io.ByteArrayOutputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

public class ImageCropModule extends ReactContextBaseJavaModule {
    private static ReactApplicationContext reactContext;
    private static final String NIK_MODEL_PATH = "tesseract/tessdata";
    private static final String NIK_LANG = "OCR+Arial";
    private final INIKReader nikReader;

    private boolean isInitialized;

    ImageCropModule(ReactApplicationContext context) {
        super(context);
        this.nikReader = new NIKReader(context);
        this.isInitialized = false;
        reactContext = context;
    }

    @Override
    public String getName() {
        return "ImageCrop";
    }

    private void openAllReader() throws IOException
    {
        this.nikReader.open(NIK_MODEL_PATH, NIK_LANG);

        this.isInitialized = true;
    }

    private void closeAllReader()
    {
        if(this.nikReader != null) this.nikReader.close();

        this.isInitialized = false;
    }

    @ReactMethod
    public void init(Promise promise)
    {
        if(this.isInitialized) promise.resolve(null);

        try
        {
            this.openAllReader();
            promise.resolve(null);
        }
        catch (Exception e)
        {
            promise.reject(e);
        }
    }

    @ReactMethod
    public void recognize(String encodedImage, Promise promise) throws IOException
    {
        if(!this.isInitialized)
        {
            try
            {
                this.openAllReader();
            }
            catch (IOException e)
            {
                promise.reject(e);
            }
        }

        WritableMap result = Arguments.createMap();
        InputStream inputStream = new FileInputStream(encodedImage.replace("file://", ""));
        Bitmap image = BitmapFactory.decodeStream(inputStream);
        int fromHere = (int) (image.getHeight() * 0.3);
        Bitmap croppedBitmap = Bitmap.createBitmap(image, 0, (int) (image.getHeight() * 0.1), (int) (image.getWidth() * 0.8), fromHere);

        // FIX ME FOR GRAYSCALE
        //Bitmap bmpGrayscale = Bitmap.createBitmap(croppedBitmap.getWidth(), croppedBitmap.getHeight(), Bitmap.Config.ARGB_8888);
        Bitmap cleanImage = ImagePreprocessor.run(croppedBitmap);
        //Canvas canvas = new Canvas(bmpGrayscale);
        //Paint paint = new Paint();
        //ColorMatrix colorMatrix = new ColorMatrix();
        //colorMatrix.setSaturation(0);
        //ColorMatrixColorFilter colorMatrixFilter = new ColorMatrixColorFilter(colorMatrix);
        //paint.setColorFilter(colorMatrixFilter);
        //canvas.drawBitmap(croppedBitmap, 0, 0, paint);

        try
        {
            String nikText = this.nikReader.read(cleanImage);

            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
            cleanImage.compress(Bitmap.CompressFormat.PNG, 100, byteArrayOutputStream);
            byte[] byteArray = byteArrayOutputStream.toByteArray();

            String encoded = Base64.encodeToString(byteArray, Base64.DEFAULT);

            result.putString("nikText", nikText);
            result.putString("newImage", encoded);

            promise.resolve(result);
        }
        catch (Exception e)
        {
            promise.reject(e);
        }
        return;
    }

    @ReactMethod
    public void destroy()
    {
        this.closeAllReader();
    }

    @Override
    public void onCatalystInstanceDestroy()
    {
        this.closeAllReader();
        super.onCatalystInstanceDestroy();
    }
}
