package com.newktp;

public enum IKTPLabel {
    FIELD("KTP:Field"),
    NIK("KTP:NIK"),
    HEAD("KTP:head");

    private String label;

    IKTPLabel(String label)
    {
        this.label = label;
    }

    public String getLabel()
    {
        return this.label;
    }
}
