package com.newktp.utils;

import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.RectF;
import android.util.Base64;

public class ImageUtils {
    private ImageUtils(){ }

    public static Bitmap fromBase64Image(String encodedImage)
    {
        byte[] decodedString = Base64.decode(encodedImage, Base64.DEFAULT);
        return BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
    }

    public static Bitmap cropImage(Bitmap imageToCrop, RectF positionToCrop)
    {
        int x, y, width, height;

        float ratio = (float) imageToCrop.getWidth() /
                Resources.getSystem().getDisplayMetrics().widthPixels;

        x = (int) (positionToCrop.left * ratio);
        y = (int) (positionToCrop.top * ratio);
        width = (int) (positionToCrop.width() * ratio);
        height = (int) (positionToCrop.height() * ratio);

        return Bitmap.createBitmap(imageToCrop, x,
                y, width, height, null, false);
    }
}
