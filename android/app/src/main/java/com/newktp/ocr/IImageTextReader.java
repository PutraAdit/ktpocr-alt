package com.newktp.ocr;

import android.content.Context;
import android.graphics.Bitmap;

public interface IImageTextReader {
    void open(Context context, String currentModelPath, String language);

    String read(Bitmap bitmap) throws Exception;

    void close();
}
