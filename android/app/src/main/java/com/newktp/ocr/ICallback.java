package com.newktp.ocr;

public interface ICallback {
    void run(String text);
}
