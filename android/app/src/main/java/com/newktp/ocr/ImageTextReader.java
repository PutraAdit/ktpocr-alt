package com.newktp.ocr;

import android.content.Context;
import android.content.res.AssetManager;
import android.graphics.Bitmap;
import android.util.Log;

import com.googlecode.leptonica.android.Pixa;
import com.googlecode.tesseract.android.ResultIterator;
import com.googlecode.tesseract.android.TessBaseAPI;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;

public class ImageTextReader implements IImageTextReader {
    private final String TAG = ImageTextReader.class.getName();
    private TessBaseAPI tesseractAPI;

    @Override
    public void open(Context context, String currentModelPath, String language)
    {
        final String tesseractPath = context.getFilesDir() + "/tesseract/";
        final String tesseractModelPath = tesseractPath + "tessdata/";

        final AssetManager assetManager = context.getAssets();

        this.tesseractAPI = new TessBaseAPI();

        File file = new File(tesseractModelPath);

        if(!file.exists())
        {
            if(!file.mkdirs())
            {
                Log.e(TAG, "Can't create path for load model!");
                return;
            }
        }

        try
        {
            InputStream modelInputFileStream = null;
            OutputStream modelOutputFileStream = null;

            String[] listModel = assetManager.list(currentModelPath);

            if(listModel != null)
            {
                for (String model : listModel)
                {
                    Log.i(TAG, model);

                    modelInputFileStream = assetManager.open(
                            currentModelPath + File.separator + model);

                    modelOutputFileStream = new FileOutputStream(
                            new File(tesseractModelPath + File.separator + model));

                    // PINDAH FILE
                    byte[] buf = new byte[1024];
                    int len;
                    while ((len = modelInputFileStream.read(buf)) != -1) {
                        modelOutputFileStream.write(buf, 0, len);
                    }

                    modelInputFileStream.close();
                    modelOutputFileStream.close();
                }
            } else {
                Log.e(TAG, "Model not found!");
            }
        } catch (IOException e) {
            Log.e(TAG, "Can't load model!", e);
            Log.getStackTraceString(e);
        }

        try {
            this.tesseractAPI.init(tesseractPath, language, TessBaseAPI.OEM_DEFAULT);
        } catch (IllegalArgumentException e) {
            Log.e(TAG, "Can't load model!", e);
            Log.getStackTraceString(e);
        } catch (Exception e) {
            Log.e(TAG, "Can't load model!", e);
            Log.getStackTraceString(e);
        }
    }

    @Override
    public String read(Bitmap bitmap) throws Exception
    {
        if(this.tesseractAPI == null) throw new Exception("Not initialized!");

        this.tesseractAPI.setImage(bitmap);
        return this.tesseractAPI.getUTF8Text();
    }

    @Override
    public void close()
    {
        if(this.tesseractAPI != null) this.tesseractAPI.end();
        this.tesseractAPI = null;
    }
}
