package com.newktp;

import android.content.Context;
import android.graphics.Bitmap;

import com.newktp.ocr.IImageTextReader;
import com.newktp.ocr.ImageTextReader;
import com.newktp.preprocessing.ImagePreprocessor;

public class NIKReader implements INIKReader {
    private static final IImageTextReader imageTextReader = new ImageTextReader();
    private final Context context;

    public NIKReader(Context context)
    {
        this.context = context;
    }

    @Override
    public void open(String modelPath, String language)
    {
        imageTextReader.open(this.context, modelPath, language);
    }

    @Override
    public String read(Bitmap image) throws Exception
    {
        //Bitmap cleanImage = ImagePreprocessor.run(image);
        return imageTextReader.read(image);
    }

    @Override
    public void close()
    {
        imageTextReader.close();
    }
}
