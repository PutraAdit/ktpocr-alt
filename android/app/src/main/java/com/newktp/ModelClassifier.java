package com.newktp;

import android.content.Context;
import android.content.res.AssetFileDescriptor;
import android.content.res.AssetManager;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.RectF;
import android.util.Log;

import com.facebook.react.bridge.ReactApplicationContext;
import com.newktp.library.Classifier;

import org.tensorflow.lite.DataType;
import org.tensorflow.lite.Interpreter;
import org.tensorflow.lite.Tensor;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.MappedByteBuffer;
import java.nio.channels.FileChannel;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Vector;

public class ModelClassifier implements IModelClassifier {
    private static final String TAG = ModelClassifier.class.getSimpleName();

    private static final int NUM_DETECTIONS = 10;
    private static final float IMAGE_MEAN = 128.0f;
    private static final float IMAGE_STD = 128.0f;

    private final Context context;

    private Interpreter tfLite;
    private int inputSize = 0;
    private Vector<String> labels;
    float[][] labelProb;

    private static final int BYTES_PER_CHANNEL = 4;

    public ModelClassifier(ReactApplicationContext context)
    {
        this.context = context;
    }

    private void loadLabels(AssetManager assetManager, String path)
    {
        BufferedReader br;
        try
        {
            br = new BufferedReader(new InputStreamReader(assetManager.open(path)));
            String line;
            labels = new Vector<>();
            while ((line = br.readLine()) != null) {
                labels.add(line);
            }
            labelProb = new float[1][labels.size()];
            br.close();
        }
        catch (IOException e)
        {
            throw new RuntimeException("Failed to read label file", e);
        }
    }

    private ByteBuffer createTensorFromImage(Bitmap imageToRecognize)
    {
        Tensor tensor = tfLite.getInputTensor(0);
        inputSize = tensor.shape()[1];
        int inputChannels = tensor.shape()[3];

        Matrix matrix = getTransformationMatrix(
                imageToRecognize.getWidth(), imageToRecognize.getHeight(),
                inputSize, inputSize, false);

        int[] intValues = new int[inputSize * inputSize];
        int bytePerChannel = tensor.dataType() == DataType.UINT8 ? 1 : BYTES_PER_CHANNEL;

        ByteBuffer imgData = ByteBuffer.allocateDirect(
                inputSize * inputSize * inputChannels * bytePerChannel);

        imgData.order(ByteOrder.nativeOrder());

        Bitmap bitmap = Bitmap.createBitmap(inputSize, inputSize, Bitmap.Config.ARGB_8888);
        final Canvas canvas = new Canvas(bitmap);
        canvas.drawBitmap(imageToRecognize, matrix, null);
        bitmap.getPixels(
                intValues, 0,
                bitmap.getWidth(), 0, 0,
                bitmap.getWidth(), bitmap.getHeight());

        int pixel = 0;
        for (int i = 0; i < inputSize; ++i)
        {
            for (int j = 0; j < inputSize; ++j)
            {
                int pixelValue = intValues[pixel++];
                if (tensor.dataType() == DataType.FLOAT32)
                {
                    imgData.putFloat((((pixelValue >> 16) & 0xFF) - IMAGE_MEAN) / IMAGE_STD);
                    imgData.putFloat((((pixelValue >> 8) & 0xFF) - IMAGE_MEAN) / IMAGE_STD);
                    imgData.putFloat(((pixelValue & 0xFF) - IMAGE_MEAN) / IMAGE_STD);
                }
                else
                {
                    imgData.put((byte) ((pixelValue >> 16) & 0xFF));
                    imgData.put((byte) ((pixelValue >> 8) & 0xFF));
                    imgData.put((byte) (pixelValue & 0xFF));
                }
            }
        }

        return imgData;
    }

    @Override
    public void open(String modelPath, String labelsPath) throws IOException
    {
        AssetManager assetManager = this.context.getAssets();
        AssetFileDescriptor fileDescriptor = assetManager.openFd(modelPath);
        FileInputStream inputStream = new FileInputStream(fileDescriptor.getFileDescriptor());
        FileChannel fileChannel = inputStream.getChannel();
        long startOffset = fileDescriptor.getStartOffset();
        long declaredLength = fileDescriptor.getDeclaredLength();
        MappedByteBuffer buffer = fileChannel.map(
                FileChannel.MapMode.READ_ONLY, startOffset, declaredLength);

        final Interpreter.Options tfliteOptions = new Interpreter.Options();
        tfliteOptions.setNumThreads(1);
        tfLite = new Interpreter(buffer, tfliteOptions);

        if (labelsPath.length() > 0)
        {
            loadLabels(assetManager, labelsPath);
        }
    }

    @Override
    public List<Classifier.Recognition> recognize(Bitmap imageToRecognize)
    {
        float[][][] outputLocations = new float[1][NUM_DETECTIONS][4];
        float[][] outputClasses = new float[1][NUM_DETECTIONS];
        float[][] outputScores = new float[1][NUM_DETECTIONS];
        float[] numDetections = new float[1];

        Object[] inputArray = {this.createTensorFromImage(imageToRecognize)};
        Map<Integer, Object> outputMap = new HashMap<>();
        outputMap.put(0, outputLocations);
        outputMap.put(1, outputClasses);
        outputMap.put(2, outputScores);
        outputMap.put(3, numDetections);

        tfLite.runForMultipleInputsOutputs(inputArray, outputMap);

        int numDetectionsOutput =
                Math.min(
                        NUM_DETECTIONS,
                        (int) numDetections[0]);

        final List<Classifier.Recognition> recognitions = new ArrayList<>(numDetectionsOutput);
        for (int i = 0; i < numDetectionsOutput; ++i)
        {
            final RectF detection =
                    new RectF(
                            outputLocations[0][i][1] * inputSize,
                            outputLocations[0][i][0] * inputSize,
                            outputLocations[0][i][3] * inputSize,
                            outputLocations[0][i][2] * inputSize);

            recognitions.add(
                    new Classifier.Recognition(
                            "" + i,
                            labels.get((int) outputClasses[0][i]),
                            outputScores[0][i], detection));
        }
        return recognitions;
    }

    @Override
    public void close()
    {
        if(tfLite != null)
        {
            try
            {
                tfLite.close();
            }
            catch (Exception e)
            {
                Log.e(TAG, "Error on closing TF Lite Module", e);
            }
        }

        tfLite = null;
        labels = null;
        labelProb = null;
    }

    private static Matrix getTransformationMatrix(final int srcWidth,
                                                  final int srcHeight,
                                                  final int dstWidth,
                                                  final int dstHeight,
                                                  final boolean maintainAspectRatio)
    {
        final Matrix matrix = new Matrix();

        if (srcWidth != dstWidth || srcHeight != dstHeight)
        {
            final float scaleFactorX = dstWidth / (float) srcWidth;
            final float scaleFactorY = dstHeight / (float) srcHeight;

            if (maintainAspectRatio)
            {
                final float scaleFactor = Math.max(scaleFactorX, scaleFactorY);
                matrix.postScale(scaleFactor, scaleFactor);
            }
            else
            {
                matrix.postScale(scaleFactorX, scaleFactorY);
            }
        }

        matrix.invert(new Matrix());
        return matrix;
    }
}
