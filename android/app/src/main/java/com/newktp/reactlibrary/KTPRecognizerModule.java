package com.newktp.reactlibrary;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Base64;

import com.facebook.react.bridge.Arguments;
import com.facebook.react.bridge.Promise;
import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.bridge.ReactContextBaseJavaModule;
import com.facebook.react.bridge.ReactMethod;
import com.facebook.react.bridge.WritableMap;
import com.newktp.IKTPLabel;
import com.newktp.IModelClassifier;
import com.newktp.INIKReader;
import com.newktp.ModelClassifier;
import com.newktp.NIKReader;
import com.newktp.library.Classifier;
import com.newktp.utils.ImageUtils;

import java.io.ByteArrayOutputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;

import androidx.annotation.NonNull;

public class KTPRecognizerModule extends ReactContextBaseJavaModule {
    private static final String KTP_MODEL_PATH = "detect.tflite";
    private static final String KTP_LABEL_PATH = "labelmap.txt";

    private static final String NIK_MODEL_PATH = "tesseract/tessdata";
    private static final String NIK_LANG = "en";

    private final IModelClassifier ktpRecognizer;
    private final INIKReader nikReader;

    private boolean isInitialized;

    KTPRecognizerModule(ReactApplicationContext context)
    {
        super(context);
        this.ktpRecognizer = new ModelClassifier(context);
        this.nikReader = new NIKReader(context);
        this.isInitialized = false;
    }

    @NonNull
    @Override
    public String getName()
    {
        return "KTPRecognizer";
    }

    private void openAllReader() throws IOException
    {
        this.ktpRecognizer.open(KTP_MODEL_PATH, KTP_LABEL_PATH);
        this.nikReader.open(NIK_MODEL_PATH, NIK_LANG);

        this.isInitialized = true;
    }

    private void closeAllReader()
    {
        if(this.ktpRecognizer != null) this.ktpRecognizer.close();
        if(this.nikReader != null) this.nikReader.close();

        this.isInitialized = false;
    }

    @ReactMethod
    public void init(Promise promise)
    {
        if(this.isInitialized) promise.resolve(null);

        try
        {
            this.openAllReader();
            promise.resolve(null);
        }
        catch (Exception e)
        {
            promise.reject(e);
        }
    }

    @ReactMethod
    public void recognize(String encodedImage, Promise promise) throws IOException
    {
        if(!this.isInitialized)
        {
            try
            {
                this.openAllReader();
            }
            catch (IOException e)
            {
                promise.reject(e);
            }
        }

        WritableMap result = Arguments.createMap();
        Classifier.Recognition nik = null;
        Bitmap nikImage;
        Bitmap nikGray;

        //Bitmap image = ImageUtils.fromBase64Image(encodedImage);
        InputStream inputStream = new FileInputStream(encodedImage.replace("file://", ""));
        Bitmap image = BitmapFactory.decodeStream(inputStream);
        List<Classifier.Recognition> classificationResults = this.ktpRecognizer.recognize(image);

        for(Classifier.Recognition item : classificationResults)
        {
            if(item.getTitle().equals(IKTPLabel.NIK.getLabel()))
            {
                nik = item;
                break;
            }
        }

        if(nik != null)
        {
            nikImage = ImageUtils.cropImage(image, nik.getLocation());
            try
            {
                String nikText = this.nikReader.read(nikImage);
                ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                nikImage.compress(Bitmap.CompressFormat.PNG, 100, byteArrayOutputStream);
                byte[] byteArray = byteArrayOutputStream .toByteArray();

                String encoded = Base64.encodeToString(byteArray, Base64.DEFAULT);

                result.putString("NIK", nikText);
                result.putString("NIKImage", encoded);

                promise.resolve(result);
            }
            catch (Exception e)
            {
                promise.reject(e);
            }
            return;
        }

        promise.reject(new Exception("The image isn't KTP!"));
    }

    @ReactMethod
    public void destroy()
    {
        this.closeAllReader();
    }

    @Override
    public void onCatalystInstanceDestroy()
    {
        this.closeAllReader();
        super.onCatalystInstanceDestroy();
    }
}
