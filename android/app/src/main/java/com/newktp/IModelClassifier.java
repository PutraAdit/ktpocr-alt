package com.newktp;

import android.graphics.Bitmap;

import com.newktp.library.Classifier;

import java.io.IOException;
import java.util.List;

public interface IModelClassifier {
    void open(final String modelPath, final String labelsPath) throws IOException;

    List<Classifier.Recognition> recognize(Bitmap imageToRecognize);

    void close();
}
