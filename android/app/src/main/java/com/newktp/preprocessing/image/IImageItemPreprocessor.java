package com.newktp.preprocessing.image;

import android.graphics.Bitmap;

import androidx.annotation.NonNull;

public interface IImageItemPreprocessor {
    Bitmap filter(@NonNull Bitmap image);
}
