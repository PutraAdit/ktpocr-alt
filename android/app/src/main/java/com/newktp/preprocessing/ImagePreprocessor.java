package com.newktp.preprocessing;

import android.graphics.Bitmap;

import com.newktp.preprocessing.image.GrayscaleFilter;
import com.newktp.preprocessing.image.HistogramEqualizerFilter;
import com.newktp.preprocessing.image.IImageItemPreprocessor;
import com.newktp.preprocessing.image.MedianFilter;
import com.newktp.preprocessing.image.ScalingFilter;
import com.newktp.preprocessing.image.ThresholdFilterer;

import java.util.Arrays;
import java.util.List;

/**
 * Image Preprocessor
 *
 * @author Mufid Jamaluddin
 */
public class ImagePreprocessor
{
    private ImagePreprocessor(){ }

    private static final List<IImageItemPreprocessor> filters =
            Arrays.asList(
                    new GrayscaleFilter(),
                    //new MedianFilter(),
                    //new HistogramEqualizerFilter(),
                    //new ContrastStretchingFilter(),
                    //new ThresholdFilterer(),
                    //new ScalingFilter()
            );

    public static Bitmap run(Bitmap dirtyImage)
    {
        Bitmap temp = dirtyImage;
        for(IImageItemPreprocessor actor : filters)
        {
            temp = actor.filter(temp);
        }
        return temp;
    }

}
