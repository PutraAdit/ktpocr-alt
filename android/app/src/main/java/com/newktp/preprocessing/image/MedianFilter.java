package com.newktp.preprocessing.image;

import android.graphics.Bitmap;
import android.graphics.Color;
import android.util.Log;

import java.util.Arrays;

import androidx.annotation.NonNull;

public class MedianFilter implements IImageItemPreprocessor {
    /**
     * Median Filter untuk memperbaiki kualitas citra dari noise
     * Implementasi dari Median Filter (Noise cahaya): https://en.wikipedia.org/wiki/Median_filter
     *
     * @param image Gambar yang akan di-filter (Udah grayscale & editable)
     * @return gambar hasil filter dalam array abu-abu (belum pixel asli)
     */
    private void medianFilter(Bitmap image)
    {
        final int windowWidth = 3, windowHeight = 3;
        final int windowLength = windowWidth * windowHeight;
        final int medianPosition = windowLength / 2;

        int[] window = new int[windowLength];

        int edgeX = windowWidth / 2;
        int edgeY = windowHeight / 2;

        int i = 0;
        int posX = 0;
        int posY = 0;
        int tempResult = 0;

        for(int x = edgeX; x < image.getWidth(); x++)
        {
            for(int y = edgeY; y < image.getHeight(); y++)
            {
                i = 0;
                for(int fx = 0; fx < windowWidth; fx++)
                {
                    for(int fy = 0; fy < windowHeight; fy++)
                    {
                        posX = x + fx - edgeX;
                        posY = y + fy - edgeY;

                        if(posX < image.getWidth() && posY < image.getHeight())
                        {
                            tempResult = Color.red(image.getPixel(posX, posY));
                        }

                        window[i] = tempResult;
                        i++;
                    }
                }

                Arrays.sort(window);
                image.setPixel(x, y,
                        Color.argb(255,
                                window[medianPosition],
                                window[medianPosition],
                                window[medianPosition]));
            }
        }
    }

    @Override
    public Bitmap filter(@NonNull Bitmap image)
    {
        try
        {
            this.medianFilter(image);
        }
        catch (Exception e)
        {
            final String TAG = MedianFilter.class.getName();
            Log.e(TAG, "Error in median filter!", e);
        }

        return image;
    }
}
