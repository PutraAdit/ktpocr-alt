package com.newktp.preprocessing.image;

import android.graphics.Bitmap;
import android.graphics.Color;

import androidx.annotation.NonNull;

public class GrayscaleFilter implements IImageItemPreprocessor {
    /**
     *  Filter Grayscale
     *
     *  @param image Gambar Awal (Editable/Non-Editable)
     *  @return Gambar yang Berisi Array Grayscale, Bukan Piksel Warna (Belum Siap)
     */
    @Override
    public Bitmap filter(@NonNull Bitmap image)
    {
        int A, R, G, B, gray, temp;
        Bitmap newImage = Bitmap.createBitmap(image.getWidth(), image.getHeight(), Bitmap.Config.ARGB_8888);

        for(int x = 0; x < image.getWidth(); x++)
        {
            for(int y = 0; y < image.getHeight(); y++)
            {
                temp = image.getPixel(x, y);

                A = Color.alpha(temp);
                R = Color.red(temp);
                G = Color.green(temp);
                B = Color.blue(temp);

                gray = (int) (0.2989 * R + 0.5870 * G + 0.1140 * B);

                newImage.setPixel(x, y, Color.argb(A, gray, gray, gray));
            }
        }

        return newImage;
    }
}
