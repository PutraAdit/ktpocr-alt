package com.newktp.preprocessing.image;

import android.graphics.Bitmap;
import android.graphics.Color;
import android.util.Log;

import androidx.annotation.NonNull;

public class HistogramEqualizerFilter implements IImageItemPreprocessor {
    private static int[] calculateHistogram(Bitmap bi)
    {
        int[] grayscaleLevels = new int[256];
        for (int i = 0; i < bi.getWidth(); i++)
        {
            for (int j = 0; j < bi.getHeight(); j++)
            {
                int pixel = bi.getPixel(i, j);
                grayscaleLevels[Color.red(pixel)]++;
            }
        }
        return grayscaleLevels;
    }

    private static void histogramEqualize(Bitmap image)
    {
        int result;
        int[] histogram = calculateHistogram(image);
        int totalData = image.getWidth() * image.getHeight();
        long sum = 0;

        float scaleFactor = (float) 255.0 / totalData;

        for (int x = 0; x < histogram.length; x++)
        {
            sum += histogram[x];
            int value = (int) (scaleFactor * sum);
            if (value > 255)
            {
                value = 255;
            }
            histogram[x] = value;
        }

        for (int i = 0; i < image.getWidth(); i++)
        {
            for (int j = 0; j < image.getHeight(); j++)
            {
                int pixel = image.getPixel(i, j);

                result = histogram[Color.red(pixel)];
                int rgb = Color.rgb(result, result, result);
                image.setPixel(i, j, rgb);
            }
        }
    }

    @Override
    public Bitmap filter(@NonNull Bitmap image)
    {
        try
        {
            histogramEqualize(image);
        }
        catch (Exception e)
        {
            final String TAG = HistogramEqualizerFilter.class.getName();
            Log.e(TAG, "Unhandled Exception!", e);
        }

        return image;
    }
}
