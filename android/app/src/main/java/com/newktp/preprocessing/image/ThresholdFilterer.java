package com.newktp.preprocessing.image;

import android.graphics.Bitmap;
import android.graphics.Color;
import android.util.Log;

import androidx.annotation.NonNull;

public class ThresholdFilterer implements IImageItemPreprocessor {
    private static String TAG = ThresholdFilterer.class.getName();

    private final int THRESHOLD = 48;
    //private long TOTAL = 0;
    //private int COUNTER = 0;
    /*
    private int MAX = 50;

    private void updateTotal(int value)
    {
        //this.TOTAL += value;
        //this.COUNTER++;

        if(this.MAX < value) this.MAX = value;
    }

    private void updateThreshold()
    {
        //this.THRESHOLD = (int) (this.TOTAL / this.COUNTER);
        //this.THRESHOLD = (this.THRESHOLD + this.MAX) / 4;
        this.THRESHOLD = this.MAX / 2;
    }
     */

    /**
     * Treshold Gambar jadi Hitam Putih
     *
     * @param image Gambar yang dilakukan Treshold
     */
    private void thresholdFilter(Bitmap image)
    {
        int gray, x, y;

        /*
        for(x = 0; x < image.getWidth(); x++)
        {
            for(y = 0; y < image.getHeight(); y++)
            {
                gray = image.getPixel(x, y);
                this.updateTotal(gray);
            }
        }
         */

        //this.updateThreshold();

        for(x = 0; x < image.getWidth(); x++)
        {
            for(y = 0; y < image.getHeight(); y++)
            {
                gray = Color.red(image.getPixel(x, y));

                if (gray < this.THRESHOLD)
                {
                    gray = Color.BLACK;
                }
                else
                {
                    gray = Color.WHITE;
                }

                image.setPixel(x, y, Color.argb(255, gray, gray, gray));
            }
        }
    }

    @Override
    public Bitmap filter(@NonNull Bitmap image)
    {
        try
        {
            this.thresholdFilter(image);
        }
        catch (Exception e)
        {
            Log.e(TAG, "Unhandled Exception!", e);
        }

        return image;
    }
}
