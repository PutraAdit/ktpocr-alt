package com.newktp.preprocessing.image;

import android.graphics.Bitmap;
import android.util.Log;

import androidx.annotation.NonNull;

public class ScalingFilter implements IImageItemPreprocessor {
    private static final int DESIRED_WIDTH = 360;

    private static Bitmap resize(Bitmap image)
    {
        double ratio = (double) DESIRED_WIDTH / image.getWidth();

        int newWidth = (int) (image.getWidth() * ratio);
        int newHeight = (int) (image.getHeight() * ratio);

        return Bitmap.createScaledBitmap(image, newWidth, newHeight, false);
    }

    @Override
    public Bitmap filter(@NonNull Bitmap image)
    {
        try
        {
            return resize(image);
        }
        catch (Exception e)
        {
            final String TAG = ScalingFilter.class.getName();
            Log.e(TAG, "Unhandled Exception!", e);
        }

        return image;
    }
}
