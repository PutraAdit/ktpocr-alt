/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React, {Component} from 'react';
import {
  Platform,
  StyleSheet,
  Image,
  Text,
  View,
  TouchableOpacity,
  Alert,
} from 'react-native';
import ImagePicker from 'react-native-image-picker';
// import ImagePicker from 'react-native-image-crop-picker';
import Module from './Module';

const height = 350;
const width = 350;
const blue = '#25d5fd';
const mobile = 'Take Picture';

export default class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      model: null,
      source: null,
      imageHeight: height,
      imageWidth: width,
      loading: false,
      nik: '',
    };
  }

  onSelectModel(model) {
    this.setState({model});
  }

  onSelectImage() {
    this.state.nik = '';
    const options = {
      title: 'Select Picture',
      storageOptions: {
        skipBackup: true,
        path: 'images',
      },
    };
    // ImagePicker.openCamera({
    //   width: 660,
    //   height: 426,
    //   cropping: true,
    // }).then((image) => {
    //   //console.log(image);
    //   var w = image.width;
    //   var h = image.height;
    //   Module.recognize(image.path)
    //     .then((result) => {
    //       this.setState((state) => {
    //         const arr = result.nikText.split(' ');
    //         console.log(arr);
    //         arr.forEach((item) => {
    //           const itemText = item.toString().replace(/[^0-9\.]/g, '');
    //           const res = itemText.substring(0, 16);
    //           if (res.length === 16) {
    //             this.state.nik = res;
    //           }
    //         });
    //         if (this.state.nik === '') {
    //           Alert.alert(
    //             'Error',
    //             'Foto KTP tidak terbaca, silahkan foto ulang',
    //             [
    //               {
    //                 text: 'Cancel',
    //                 onPress: () => console.log('Cancel Pressed'),
    //                 style: 'cancel',
    //               },
    //               {text: 'OK', onPress: () => console.log('OK Pressed')},
    //             ],
    //             {cancelable: false},
    //           );
    //           return {
    //             ...state,
    //             source: null,
    //           };
    //         }

    //         return {
    //           ...state,
    //           source: {uri: `data:image/png;base64,${result.newImage}`},
    //           imageHeight: 426,
    //           imageWidth: 660,
    //         };
    //       });
    //     })
    //     .catch((err) => {
    //       Alert.alert(
    //         'Error',
    //         err.message,
    //         [
    //           {
    //             text: 'Cancel',
    //             onPress: () => console.log('Cancel Pressed'),
    //             style: 'cancel',
    //           },
    //           {text: 'OK', onPress: () => console.log('OK Pressed')},
    //         ],
    //         {cancelable: false},
    //       );
    //       this.setState((previousState) => ({
    //         ...previousState,
    //         loading: false,
    //       }));
    //     });
    // });
    ImagePicker.showImagePicker(options, (response) => {
      if (response.didCancel) {
        console.log('User cancelled image picker');
      } else if (response.error) {
        console.log('ImagePicker Error: ', response.error);
      } else {
        var path =
          Platform.OS === 'ios' ? response.uri : 'file://' + response.path;
        var w = response.width;
        var h = response.height;
        Module.recognize(path)
          .then((result) => {
            this.setState((state) => {
              const arr = result.nikText.split(" ");
              arr.forEach((item) => {
                const itemText = item.toString().replace(/[^0-9\.]/g, '');
                const res = itemText.substring(0, 16);
                if (res.length === 16) {
                  this.state.nik = res;
                }
              });
              if (this.state.nik === '') {
                Alert.alert(
                  'Error',
                  'Foto KTP tidak terbaca, silahkan foto ulang',
                  [
                    {
                      text: 'Cancel',
                      onPress: () => console.log('Cancel Pressed'),
                      style: 'cancel',
                    },
                    {text: 'OK', onPress: () => console.log('OK Pressed')},
                  ],
                  {cancelable: false},
                );
                return {
                  ...state,
                  source: null,
                };
              }

              return {
                ...state,
                source: {uri: `data:image/png;base64,${result.newImage}`},
                imageHeight: (h * width) / w,
                imageWidth: width,
              };
            });
          })
          .catch((err) => {
            Alert.alert(
              'Error',
              err.message,
              [
                {
                  text: 'Cancel',
                  onPress: () => console.log('Cancel Pressed'),
                  style: 'cancel',
                },
                {text: 'OK', onPress: () => console.log('OK Pressed')},
              ],
              {cancelable: false},
            );
            this.setState((previousState) => ({
              ...previousState,
              loading: false,
            }));
          });
      }
    });
  }

  renderResults() {
    const {nik} = this.state;
    return <Text style={{color: 'black'}}>{nik}</Text>;
  }

  render() {
    const {model, source, imageHeight, imageWidth} = this.state;
    var renderButton = (m) => {
      return (
        <TouchableOpacity
          style={styles.button}
          onPress={this.onSelectModel.bind(this, m)}>
          <Text style={styles.buttonText}>{m}</Text>
        </TouchableOpacity>
      );
    };
    return (
      <View style={styles.container}>
        {model ? (
          <TouchableOpacity
            style={[
              styles.imageContainer,
              {
                height: imageHeight,
                width: imageWidth,
                borderWidth: source ? 0 : 2,
              },
            ]}
            onPress={this.onSelectImage.bind(this)}>
            {source ? (
              <Image
                source={source}
                style={{
                  height: imageHeight,
                  width: imageWidth,
                }}
                resizeMode="contain"
              />
            ) : (
              <Text style={styles.text}>Select Picture</Text>
            )}
            <View style={styles.boxes}>{this.renderResults()}</View>
          </TouchableOpacity>
        ) : (
          <View>{renderButton(mobile)}</View>
        )}
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'white',
  },
  imageContainer: {
    borderColor: blue,
    borderRadius: 5,
    alignItems: 'center',
  },
  text: {
    color: blue,
  },
  button: {
    width: 200,
    backgroundColor: blue,
    borderRadius: 10,
    height: 40,
    alignItems: 'center',
    justifyContent: 'center',
    marginBottom: 10,
  },
  buttonText: {
    color: 'white',
    fontSize: 15,
  },
  box: {
    position: 'absolute',
    borderColor: blue,
    borderWidth: 2,
  },
  boxes: {
    position: 'absolute',
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'white',
  },
});
